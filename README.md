# Getting Things Done

`App::GTD` is a free and open source application written in the [Perl 6
programming language](https://perl6.org/). It is intended to support the
["Getting Things Done"](https://gettingthingsdone.com/what-is-gtd/)
methodology.

## Installation

You will need to have Perl 6 installed. You may need to consult your
distribution's package manager to do this.

### Installing from the repository

Clone this repository and resolve the dependencies.

    git clone https://gitlab.com/tyil/perl6-app-gtd.git
    cd perl6-app-gtd
    zef install --/test --deps-only .

After this, you can choose to install `App::GTD` as a package with `zef`, or
just invoke it from the repository. To install it with `zef`, run the `install`
command again.

    zef install .

To run the program from the repository, call `perl6` and include the `lib`
directory in the search path.

    perl6 -Ilib bin/gtd

### Configuration

You will have to write a configuration file to make use of it. Since this is an
early stage, only a filesystem directory is supported as a database.

The configuration file is read from `$XDG_CONFIG_HOME/gtd/config.toml`. If
`$XDG_CONFIG_HOME` is not set, it will default to `$HOME/.config`. The minimal
configuration to use the directory `/home/tyil/gtd` as the location to store
it's information is only 3 lines.

    [database]
    type = "fs"
    directory = "/home/tyil/gtd"

Using `~` to refer to the home directory is not (yet!) supported.

## Using `gtd`

If you invoke `gtd` without any arguments, it will show a list of available
commands. The most basic actions you will want are `add`, `inbox` and `next`.

### Adding things to your inbox

Using the GTD methodology, you will be adding things to your inbox throughout
the day. This is accomplished by using the `add` subcommand.

    gtd add "some thing I must not forget"
    gtd add learn more about gtd

You don't need to quote the item you want to add, unless you want to conserve
multiple spaces in the resulting item.

### Listing the entries in your inbox

To display the items that have built up in your inbox, the subcommand `inbox`
exists.

    gtd inbox

This will show a list of items, starting with their ID. You will need this to
work with the items.

### Creating a Next Item from an inbox item

To create a Next Item, you use the `next` subcommand. It's first argument is
the ID of the Inbox Item you want to turn into a Next Item. Optionally, you can
alter the label of the item by adding a new one after the ID.

    gtd next 1 Commit the perl6-App-GTD README
    gtd next 2

### Listing Next Items

To list all your Next Items, call `next` without any arguments.

    gtd next

This list looks similar to the Inbox Items list.

### Marking an item as Done

Lastly, you will be finishing off items and should mark them as such. In this
application, marking an Item as Done will remove it from the database. For this
you can use the aptly named `done` subcommand.

    gtd done 1

## Related tools

### Awesome plugin

For people using the [Awesome](https://awesomewm.org/) window manager, there is
a plugin to show your Next Items when hovering over the clock/calendar. This
plugin is freely available [from
GitLab](https://gitlab.com/tyil/lua-gtd-awesome)

## License

This program is distributed under the terms of the GNU Affero General Public
License, version 3.
