#! /usr/bin/env false

use v6.d;

use DB::SQLite;

use App::GTD::Config;
use App::GTD::Model;

unit class App::GTD::DB;

my %connections;

method connection (
	'sqlite',
	--> DB::SQLite::Connection
) {
	self.dbh('sqlite').db
}

method dbh (
	'sqlite',
	--> DB::SQLite
) {
	if (!%connections<sqlite>) {
		%connections<sqlite> = DB::SQLite.new(
			filename => config('database.file'),
			busy-timeout => 5_000,
		)
	}

	%connections<sqlite>;
}

method model (
	Str:D $name,
	--> App::GTD::Model
) {
	my $full-name = 'App::GTD::Models::%s::%s'.sprintf(
		config('database.type').tclc,
		$name,
	);

	require ::($full-name);
	return ::($full-name);
}

=begin pod

=NAME    App::GTD::DB
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
