#! /usr/bin/env false

use v6.d;

use String::Fold;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::AddProject;

constant DB = App::GTD::DB;

#| Add a new project.
multi sub MAIN (
	"project",
	"add",

	#| The label to use for the new project.
	*@label,
) is export {
	config-load;

	my $project = DB.model('Project').new(
		label => @label.join(' '),
	).INSERT;

	"\"{$project.label}\" added as a Project.".&fold.say;
}

=begin pod

=NAME    App::GTD::Bin::AddProject
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
