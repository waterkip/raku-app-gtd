#! /usr/bin/env false

use v6.d;

use String::Fold;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::Done;

constant DB = App::GTD::DB;

#| Mark a Thing as done.
multi sub MAIN (
	"done",

	#| The ID of the Thing to operate on.
	Int:D $id!,

	*@ids,
) is export {
	config-load;

	@ids .= prepend($id);

	for @ids -> $id {
		my $thing = DB.model('Thing').get($id);

		if (!$thing) {
			"There was no Thing with ID $id found.".say;
			next;
		}

		$thing.DELETE;

		"\"{$thing.label}\" has been removed from your list.".&fold.say;
	}
}

=begin pod

=NAME    App::GTD::Bin::Done
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
