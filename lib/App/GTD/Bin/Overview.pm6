#! /usr/bin/env false

use v6.d;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::Overview;

constant DB = App::GTD::DB;

#| Show an overview of your GTD life.
multi sub MAIN (
	"overview"
) is export {
	config-load;

	my $inbox-count = DB.model('Thing').get-inbox.elems;
	my @next-items = DB.model('Thing').get-next;
	my @next-general = @next-items.grep(!*.project-id);
	my $next-project-count = @next-items.elems - @next-general.elems;
	my @projects = DB.model('Project').get-all;
	my $output;

	$output ~= "Inbox: $inbox-count\n";
	$output ~= "Next Items: {@next-general.elems} (+$next-project-count in projects)\n";

	my $longest-id = @next-general.sort(*.id).tail.id.chars;
	my $longest-label = @next-general.sort(*.label.chars).tail.label.chars;

	my $format = "[%{$longest-id}d] %-{$longest-label}s @%s (%8s)\n";

	for @next-general -> $thing {
		$output ~= $format.sprintf(
			$thing.id,
			$thing.label,
			$thing.context,
			$thing.created-at.Date,
		)
	}

	$output ~= " \nProjects\n";

	my $longest-project-id = @projects.sort(*.id).tail.id.chars;
	my $longest-project-label = @projects.sort(*.label.chars).tail.label.chars;
	my $project-format = "[%{$longest-project-id}d] %-{$longest-project-label}s (%8s)\n";

	for @projects -> $project {
		$output ~= $project-format.sprintf(
			$project.id,
			$project.label,
			$project.created-at.Date,
		);

		for $project.next -> $thing {
			$output ~= $format.indent($longest-project-id + 3).sprintf(
				$thing.id,
				$thing.label,
				$thing.context,
				$thing.created-at.Date,
			);
		}
	}

	$output.say;
}

=begin pod

=NAME    App::GTD::Bin::Overview
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
