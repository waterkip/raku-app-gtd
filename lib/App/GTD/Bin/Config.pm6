#! /usr/bin/env false

use v6.d;

use Data::Dump;
use JSON::Fast;

use App::GTD::Config;

unit module App::GTD::Bin::Config;

#| Show the current configuration. This is mainly useful for debugging
#| purposes.
multi sub MAIN (
	"config",
) is export {
	config-load;

	say(Dump(config(), :skip-methods));
}

multi sub MAIN (
	"config",
	Bool :$json! where { $_ },
) is export {
	config-load;

	say(to-json(config.get, :sorted-keys))
}

=begin pod

=NAME    App::GTD::Bin::Config
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
