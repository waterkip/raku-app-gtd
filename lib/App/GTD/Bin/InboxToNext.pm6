#! /usr/bin/env false

use v6.d;

use String::Fold;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::InboxToNext;

constant DB = App::GTD::DB;

#| Move a Thing from your Inbox to your Next Items.
multi sub MAIN (
	"next",

	#| The ID of the Thing to operate on.
	Int:D $id!,

	#| The label to use for the new Thing.
	*@label,

	#| The context to use for the Next Item.
	Str :$context,

	Int :$project,
) is export {
	config-load;

	my $thing = DB.model('Thing').get($id);

	if (!$thing) {
		"There was no Thing with ID $id found.".say;
		return;
	}

	$thing.state = 'next';
	$thing.updated-at = DateTime.now;
	$thing.label = @label.join(' ') if @label;
	$thing.project-id = $project if $project;
	$thing.context = $context if $context;

	$thing.UPDATE;

	"\"{$thing.label}\" has been added as a Next item.".&fold.say;
}

=begin pod

=NAME    App::GTD::Bin::InboxToNext
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
