#! /usr/bin/env false

use v6.d;

use String::Fold;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::Add;

constant DB = App::GTD::DB;

#| Add a new Thing to your inbox.
multi sub MAIN (
	"add",

	#| The label to use for new Thing.
	*@label where *.elems > 0,

	#| The context to use for the Inbox Item.
	Str :$context,

	#| Add the new entry directly to your Next Items.
	Bool:D :$next = False,

	#| Add the item directly to a project. Only applies if --next is used.
	Int :$project,
) is export {
	config-load;

	my $thing = DB.model('Thing').new(
		label => @label.join(' '),
		state => $next ?? 'next' !! 'inbox',
		context => $context // config('gtd.context'),
	);

	if ($next && $project) {
		$thing.project-id = $project;
	}

	$thing.INSERT;

	"\"{$thing.label}\" added to your Inbox.".&fold.say;
}

=begin pod

=NAME    App::GTD::Bin::Add
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
