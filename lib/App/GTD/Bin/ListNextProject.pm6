#! /usr/bin/env false

use v6.d;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::ListNextProject;

constant DB = App::GTD::DB;

#| List your Next Items for a particular Project, with oldest entries on top.
multi sub MAIN (
	"next",

	Int:D :$project!,
) is export {
	config-load;

	my @things = DB.model('Project').get($project).next;

	if (!@things) {
		"You're all out of Next actions for Project#$project!".say;
		return;
	}

	my $longest-id = @things.sort(*.id).tail.id.chars;
	my $longest-label = @things.sort(*.label.chars).tail.label.chars;
	my $format = '[%' ~ $longest-id ~ 'd] %-' ~ $longest-label ~ 's (%8s)';

	for @things -> $thing {
		$format
			.sprintf(
				$thing.id,
				$thing.label,
				$thing.created-at.Date,
			)
			.say
	};
}

=begin pod

=NAME    App::GTD::Bin::ListNext
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
