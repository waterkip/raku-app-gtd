#! /usr/bin/env false

use v6.d;

use App::GTD::Config;
use App::GTD::DB;

unit module App::GTD::Bin::ListProjects;

constant DB = App::GTD::DB;

#| List your Projects, with oldest entries on top.
multi sub MAIN (
	"project",
) is export {
	config-load;

	my @projects = DB.model('Project').get-all;

	if (!@projects) {
		"You have no running projects.".say;
		return;
	}

	my $longest-id = @projects.sort(*.id).tail.id.chars;
	my $longest-label = @projects.sort(*.label.chars).tail.label.chars;
	my $format = '[%' ~ $longest-id ~ 'd] %-' ~ $longest-label ~ 's (%d Things) (%8s)';

	for @projects -> $project {
		$format
			.sprintf(
				$project.id,
				$project.label,
				$project.next.elems,
				$project.created-at.Date,
			)
			.say
	};
}

=begin pod

=NAME    App::GTD::Bin::ListProjects
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
