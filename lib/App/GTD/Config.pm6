#! /usr/bin/env false

use v6.d;

use Config;
use IO::Path::XDG;

unit module App::GTD::Config;

my Config $config .= new.read: {
	gtd => {
		context => 'default',
	},
	database => {
		type => 'fs',
		directory => xdg-data-home.add('gtd/data').absolute,
	},
};

sub config-file (
	--> IO::Path
) is export {
	xdg-config-home.add('gtd/config.toml')
}

sub config-load () is export
{
	my $file = config-file;

	return unless $file.e;

	$config.=read($file.absolute);

	return;
}

multi sub config (
	--> Config
) is export {
	$config
}

multi sub config (
	Str:D $key,
	--> Any
) is export {
	$config.get($key)
}

=begin pod

=NAME    App::GTD::Config
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
