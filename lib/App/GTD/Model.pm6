#! /usr/bin/env false

use v6.d;

unit role App::GTD::Model;

method DELETE () { ... }
method INSERT () { ... }
method UPDATE () { ... }

method Hash ()
{
	my %data;

	for self.^attributes(:local) {
		%data{$_.Str.substr(2..*)} = $_.get_value(self);
	}

	%data;
}

=begin pod

=NAME    App::GTD::Model
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
