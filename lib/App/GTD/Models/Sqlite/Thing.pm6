#! /usr/bin/env false

use v6.d;

use App::GTD::DB;
use App::GTD::Models::Thing;

unit class App::GTD::Models::Sqlite::Thing does App::GTD::Models::Thing;

constant DB = App::GTD::DB;

method DELETE ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		DELETE FROM things WHERE id = ?
		STMT

	$stmt.execute(
		$.id,
		:finish,
	);

	self;
}

method INSERT ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		INSERT INTO things (
			label,
			description,
			state,
			project_id,
			created_at,
			updated_at
		)
		VALUES (?, ?, ?, ?, ?, ?)
		STMT

	$stmt.execute(
		$.label,
		$.description,
		$.state,
		$.project-id,
		DateTime.now.Str,
		DateTime.now.Str,
		:finish,
	);

	self;
}

method UPDATE ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		UPDATE things SET
			label = ?,
			description = ?,
			state = ?,
			project_id = ?,
			created_at = ?,
			updated_at = ?
		WHERE id = ?
		STMT

	$stmt.execute(
		$.label,
		$.description,
		$.state,
		$.project-id,
		$.created-at.Str,
		$.updated-at.Str,
		$.id,
		:finish,
	);

	self;
}

method get (
	Int:D $id,
) {
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		SELECT *
		FROM things
		WHERE id = ?
		STMT

	my $result = $stmt
		.execute(
			$id,
			:finish,
		)
		.hash
		;

	return fail("No Thing#$id") if !$result;

	self!record-to-model($result);
}

method get-inbox ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		SELECT *
		FROM things
		WHERE state = ?
		ORDER BY created_at ASC
		STMT

	$stmt
		.execute(
			'inbox',
			:finish,
		)
		.hashes
		.map({ self!record-to-model($_) })
		;
}

method get-next ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		SELECT *
		FROM things
		WHERE state = ?
		AND project_id IS NULL
		ORDER BY created_at ASC
		STMT

	$stmt
		.execute(
			'next',
			:finish,
		)
		.hashes
		.map({ self!record-to-model($_) })
		;
}

method get-next-for-project (
	Int:D $id,
) {
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		SELECT *
		FROM things
		WHERE state = ?
		AND project_id = ?
		ORDER BY created_at ASC
		STMT

	$stmt
		.execute(
			'next',
			$id,
			:finish,
		)
		.hashes
		.map({ self!record-to-model($_) })
		;
}

method !record-to-model (
	%record
) {
	self.new(
		id => %record<id>,
		label => %record<label>,
		description => %record<description>,
		state => %record<state>,
		project-id => %record<project_id> ~~ Int ?? %record<project_id> !! Int,
		created-at => DateTime.new(%record<created_at>),
		updated-at => DateTime.new(%record<updated_at>),
	)
}

=begin pod

=NAME    App::GTD::Models::Sqlite::Thing
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
