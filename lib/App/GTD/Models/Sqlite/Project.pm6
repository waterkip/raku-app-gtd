#! /usr/bin/env false

use v6.d;

use App::GTD::DB;
use App::GTD::Models::Project;

unit class App::GTD::Models::Sqlite::Project does App::GTD::Models::Project;

constant DB = App::GTD::DB;

method DELETE ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		DELETE FROM projects WHERE id = ?
		STMT

	$stmt.execute(
		$.id,
		:finish,
	);

	self;
}

method INSERT ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		INSERT INTO projects (label, description, created_at, updated_at)
		VALUES (?, ?, ?, ?)
		STMT

	$stmt.execute(
		$.label,
		$.description,
		DateTime.now.Str,
		DateTime.now.Str,
		:finish,
	);

	self;
}

method UPDATE ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		UPDATE projects SET
			label = ?,
			description = ?,
			created_at = ?,
			updated_at = ?
		WHERE id = ?
		STMT

	$stmt.execute(
		$.label,
		$.description,
		$.created-at.Str,
		$.updated-at.Str,
		$.id,
		:finish,
	);

	self;
}

method get-all ()
{
	my $stmt = DB.connection('sqlite').prepare(q:to/STMT/);
		SELECT * FROM projects
		STMT

	$stmt
		.execute(
			:finish,
		)
		.hashes
		.map({ self!record-to-model($_) })
		;
}

method next ()
{
	DB.model('Thing').get-next-for-project($.id)
}

method !record-to-model (
	%record
) {
	self.new(
		id => %record<id>,
		label => %record<label>,
		description => %record<description>,
		created-at => DateTime.new(%record<created_at>),
		updated-at => DateTime.new(%record<updated_at>),
	)
}

=begin pod

=NAME    App::GTD::Models::Sqlite::Project
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
