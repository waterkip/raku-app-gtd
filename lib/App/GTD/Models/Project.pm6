#! /usr/bin/env false

use v6.d;

use App::GTD::Model;

unit role App::GTD::Models::Project does App::GTD::Model;

has Int $.id;
has Str $.label is rw = '';
has Str $.description is rw = '';
has DateTime $.created-at;
has DateTime $.updated-at is rw;

method get-all () { ... }

=begin pod

=NAME    App::GTD::Models::Project
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
