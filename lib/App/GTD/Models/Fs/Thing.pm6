#! /usr/bin/env false

use v6.d;

use App::GTD::Config;
use App::GTD::Models::Thing;

use JSON::Fast;

unit class App::GTD::Models::Fs::Thing does App::GTD::Models::Thing;

method DELETE ()
{
	self!path.unlink
}

method INSERT ()
{
	$!id = self!id;
	$!created-at = DateTime.now;
	$!updated-at = DateTime.now;

	self!dir.mkdir;
	self!path.spurt(to-json(self.Hash, :sorted-keys));

	self;
}

method UPDATE ()
{
	$!updated-at = DateTime.now;

	self!dir.mkdir;
	self!path.spurt(to-json(self.Hash, :sorted-keys));

	self;
}

method get (
	Int:D $id,
) {
	my $file = config('database.directory').IO.add("things/$id.json");

	return fail("No Thing#$id") if !$file.f;

	self!record-to-model($file.slurp.&from-json);
}

multi method get-next(
	Str:D :$context = 'default',
) {
	self!fetch-all.grep(*.state eq 'next').grep(*.context eq $context)
}

multi method get-next(
	Str:D :$context = 'default',
	Bool:D :$no-project-id where { $_ } = False,
) {
	self.get-next(:$context).grep(!*.project-id)
}

method get-inbox ()
{
	self!fetch-all.grep(*.state eq 'inbox')
}

method !dir (
	--> IO::Path
) {
	config('database.directory').IO.add('things');
}

method !fetch-all (
	--> Seq
) {
	return ().Seq unless self!dir.d;

	self!dir
		.dir(:test(*.f && !*.starts-with('.')))
		.map({ self!record-to-model($_.slurp.&from-json) })
}

method !id ()
{
	my $id-file = self!dir.add('.sequence');
	my $next = $id-file.f ?? $id-file.slurp.Int !! 0;

	self!dir.mkdir;
	$id-file.spurt(++$next);

	$next;
}

method !path (
	--> IO::Path
) {
	self!dir.add($!id ~ '.json')
}

method !record-to-model (
	%record
) {
	self.new(
		id => %record<id>,
		label => %record<label>,
		description => %record<description>,
		state => %record<state>,
		context => %record<context> // 'default',
		project-id => %record<project-id> // Int,
		created-at => DateTime.new(%record<created-at>),
		updated-at => DateTime.new(%record<updated-at>),
	)
}

=begin pod

=NAME    App::GTD::Models::Fs::Thing
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
