-- Revert gtd-api:projects from sqlite

BEGIN;

DROP TABLE projects;

COMMIT;
