-- Revert gtd-api:things from sqlite

BEGIN;

DROP TABLE things;

COMMIT;
