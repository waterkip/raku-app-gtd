-- Verify gtd-api:projects on sqlite

BEGIN;

SELECT
	id,
	label,
	description,
	created_at,
	updated_at
FROM projects
WHERE 0;

ROLLBACK;
