-- Verify gtd-api:things on sqlite

BEGIN;

SELECT
	id,
	label,
	description,
	project_id,
	created_at,
	updated_at
FROM things
WHERE 0;

ROLLBACK;
