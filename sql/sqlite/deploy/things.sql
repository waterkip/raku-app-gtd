-- Deploy gtd-api:things to sqlite

BEGIN;

CREATE TABLE things (
	id INTEGER PRIMARY KEY ASC,
	label TEXT NOT NULL,
	description TEXT NOT NULL,
	state TEXT NOT NULL,
	project_id INTEGER,
	created_at TEXT NOT NULL,
	updated_at TEXT NOT NULL
);

COMMIT;
