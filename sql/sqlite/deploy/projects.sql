-- Deploy gtd-api:projects to sqlite

BEGIN;

CREATE TABLE projects (
	id INTEGER PRIMARY KEY ASC,
	label TEXT NOT NULL,
	description TEXT NOT NULL,
	created_at TEXT NOT NULL,
	updated_at TEXT NOT NULL
);

COMMIT;
